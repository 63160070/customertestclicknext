﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class PostCode
    {
        public PostCode()
        {
            Customers = new HashSet<Customer>();
        }

        public int PostCodeId { get; set; }
        public string PostCode1 { get; set; } = null!;
        public int SubDistrictId { get; set; }

        public virtual SubDistrict SubDistrict { get; set; } = null!;
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
