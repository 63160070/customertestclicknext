﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class Province
    {
        public Province()
        {
            Customers = new HashSet<Customer>();
            Districts = new HashSet<District>();
        }

        public int ProvinceId { get; set; }
        public string Province1 { get; set; } = null!;

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<District> Districts { get; set; }
    }
}
