﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class CustomerTag
    {
        public Guid CustomerTagId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid TagId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
        public virtual Tag Tag { get; set; } = null!;
    }
}
