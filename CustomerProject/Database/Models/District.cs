﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class District
    {
        public District()
        {
            Customers = new HashSet<Customer>();
            SubDistricts = new HashSet<SubDistrict>();
        }

        public int DistrictId { get; set; }
        public string District1 { get; set; } = null!;
        public int ProvinceId { get; set; }

        public virtual Province Province { get; set; } = null!;
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<SubDistrict> SubDistricts { get; set; }
    }
}
