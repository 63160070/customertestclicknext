﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class Phone
    {
        public Guid PhoneId { get; set; }
        public string PhoneNumber { get; set; } = null!;
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
    }
}
