﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class Tag
    {
        public Tag()
        {
            CustomerTags = new HashSet<CustomerTag>();
        }

        public Guid TagId { get; set; }
        public string Tag1 { get; set; } = null!;

        public virtual ICollection<CustomerTag> CustomerTags { get; set; }
    }
}
