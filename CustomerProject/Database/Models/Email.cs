﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class Email
    {
        public Guid EmailId { get; set; }
        public string EmailAddress { get; set; } = null!;
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
    }
}
