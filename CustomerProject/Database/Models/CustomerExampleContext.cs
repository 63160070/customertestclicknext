﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CustomerProject.Database.Models
{
    public partial class CustomerExampleContext : DbContext
    {
        public CustomerExampleContext()
        {
        }

        public CustomerExampleContext(DbContextOptions<CustomerExampleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; } = null!;
        public virtual DbSet<CustomerTag> CustomerTags { get; set; } = null!;
        public virtual DbSet<District> Districts { get; set; } = null!;
        public virtual DbSet<Email> Emails { get; set; } = null!;
        public virtual DbSet<Phone> Phones { get; set; } = null!;
        public virtual DbSet<PostCode> PostCodes { get; set; } = null!;
        public virtual DbSet<Province> Provinces { get; set; } = null!;
        public virtual DbSet<SubDistrict> SubDistricts { get; set; } = null!;
        public virtual DbSet<Tag> Tags { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=NASAN-LAPTOP;Initial Catalog=CustomerExample;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customer");

                entity.Property(e => e.CustomerId)
                    .ValueGeneratedNever()
                    .HasColumnName("customer_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.Building)
                    .HasMaxLength(100)
                    .HasColumnName("building");

                entity.Property(e => e.CustomerCode)
                    .HasMaxLength(255)
                    .HasColumnName("customer_code");

                entity.Property(e => e.CustomerType).HasColumnName("customer_type");

                entity.Property(e => e.District)
                    .HasMaxLength(100)
                    .HasColumnName("district");

                entity.Property(e => e.DistrictId).HasColumnName("district_id");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(100)
                    .HasColumnName("first_name");

                entity.Property(e => e.IdCard)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasColumnName("id_card");

                entity.Property(e => e.LastName)
                    .HasMaxLength(100)
                    .HasColumnName("last_name");

                entity.Property(e => e.Moo)
                    .HasMaxLength(30)
                    .HasColumnName("moo");

                entity.Property(e => e.PostCode)
                    .HasMaxLength(50)
                    .HasColumnName("post_code");

                entity.Property(e => e.PostCodeId).HasColumnName("post_code_id");

                entity.Property(e => e.PrefixName)
                    .HasMaxLength(50)
                    .HasColumnName("prefix_name");

                entity.Property(e => e.Province)
                    .HasMaxLength(100)
                    .HasColumnName("province");

                entity.Property(e => e.ProvinceId).HasColumnName("province_id");

                entity.Property(e => e.Road)
                    .HasMaxLength(100)
                    .HasColumnName("road");

                entity.Property(e => e.Soi)
                    .HasMaxLength(100)
                    .HasColumnName("soi");

                entity.Property(e => e.SubDistrict)
                    .HasMaxLength(100)
                    .HasColumnName("sub_district");

                entity.Property(e => e.SubDistrictId).HasColumnName("sub_district_id");

                entity.HasOne(d => d.DistrictNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK__customer__distri__59FA5E80");

                entity.HasOne(d => d.PostCodeNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.PostCodeId)
                    .HasConstraintName("FK__customer__post_c__5BE2A6F2");

                entity.HasOne(d => d.ProvinceNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK__customer__provin__5AEE82B9");

                entity.HasOne(d => d.SubDistrictNavigation)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.SubDistrictId)
                    .HasConstraintName("FK__customer__sub_di__59063A47");
            });

            modelBuilder.Entity<CustomerTag>(entity =>
            {
                entity.ToTable("customer_tag");

                entity.Property(e => e.CustomerTagId)
                    .ValueGeneratedNever()
                    .HasColumnName("customer_tag_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.TagId).HasColumnName("tag_id");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerTags)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__customer___custo__5EBF139D");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.CustomerTags)
                    .HasForeignKey(d => d.TagId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__customer___tag_i__5FB337D6");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.ToTable("district");

                entity.Property(e => e.DistrictId)
                    .ValueGeneratedNever()
                    .HasColumnName("district_id");

                entity.Property(e => e.District1)
                    .HasMaxLength(100)
                    .HasColumnName("district");

                entity.Property(e => e.ProvinceId).HasColumnName("province_id");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Districts)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__district__provin__60A75C0F");
            });

            modelBuilder.Entity<Email>(entity =>
            {
                entity.ToTable("email");

                entity.Property(e => e.EmailId)
                    .ValueGeneratedNever()
                    .HasColumnName("email_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(50)
                    .HasColumnName("email_address");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Emails)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__email__customer___5DCAEF64");
            });

            modelBuilder.Entity<Phone>(entity =>
            {
                entity.ToTable("phone");

                entity.Property(e => e.PhoneId)
                    .ValueGeneratedNever()
                    .HasColumnName("phone_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(20)
                    .HasColumnName("phone_number");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Phones)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__phone__customer___5CD6CB2B");
            });

            modelBuilder.Entity<PostCode>(entity =>
            {
                entity.ToTable("post_code");

                entity.Property(e => e.PostCodeId)
                    .ValueGeneratedNever()
                    .HasColumnName("post_code_id");

                entity.Property(e => e.PostCode1)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("post_code");

                entity.Property(e => e.SubDistrictId).HasColumnName("sub_district_id");

                entity.HasOne(d => d.SubDistrict)
                    .WithMany(p => p.PostCodes)
                    .HasForeignKey(d => d.SubDistrictId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__post_code__sub_d__628FA481");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("province");

                entity.Property(e => e.ProvinceId)
                    .ValueGeneratedNever()
                    .HasColumnName("province_id");

                entity.Property(e => e.Province1)
                    .HasMaxLength(100)
                    .HasColumnName("province");
            });

            modelBuilder.Entity<SubDistrict>(entity =>
            {
                entity.ToTable("sub_district");

                entity.Property(e => e.SubDistrictId)
                    .ValueGeneratedNever()
                    .HasColumnName("sub_district_id");

                entity.Property(e => e.DistrictId).HasColumnName("district_id");

                entity.Property(e => e.SubDistrict1)
                    .HasMaxLength(100)
                    .HasColumnName("sub_district");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.SubDistricts)
                    .HasForeignKey(d => d.DistrictId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__sub_distr__distr__619B8048");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.ToTable("tag");

                entity.Property(e => e.TagId)
                    .ValueGeneratedNever()
                    .HasColumnName("tag_id");

                entity.Property(e => e.Tag1)
                    .HasMaxLength(50)
                    .HasColumnName("tag");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
