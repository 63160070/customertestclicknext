﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerTags = new HashSet<CustomerTag>();
            Emails = new HashSet<Email>();
            Phones = new HashSet<Phone>();
        }

        public Guid CustomerId { get; set; }
        public int CustomerType { get; set; }
        public string PrefixName { get; set; } = null!;
        public string CustomerCode { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string? LastName { get; set; }
        public string IdCard { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string? Moo { get; set; }
        public string? Building { get; set; }
        public string? Soi { get; set; }
        public string? Road { get; set; }
        public int? SubDistrictId { get; set; }
        public string? SubDistrict { get; set; }
        public int? DistrictId { get; set; }
        public string? District { get; set; }
        public int? ProvinceId { get; set; }
        public string? Province { get; set; }
        public int? PostCodeId { get; set; }
        public string? PostCode { get; set; }

        public virtual District? DistrictNavigation { get; set; }
        public virtual PostCode? PostCodeNavigation { get; set; }
        public virtual Province? ProvinceNavigation { get; set; }
        public virtual SubDistrict? SubDistrictNavigation { get; set; }
        public virtual ICollection<CustomerTag> CustomerTags { get; set; }
        public virtual ICollection<Email> Emails { get; set; }
        public virtual ICollection<Phone> Phones { get; set; }
    }
}
