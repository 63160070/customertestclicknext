﻿using System;
using System.Collections.Generic;

namespace CustomerProject.Database.Models
{
    public partial class SubDistrict
    {
        public SubDistrict()
        {
            Customers = new HashSet<Customer>();
            PostCodes = new HashSet<PostCode>();
        }

        public int SubDistrictId { get; set; }
        public string SubDistrict1 { get; set; } = null!;
        public int DistrictId { get; set; }

        public virtual District District { get; set; } = null!;
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<PostCode> PostCodes { get; set; }
    }
}
