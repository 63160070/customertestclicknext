using CustomerProject.BusinessLogics;
using CustomerProject.Database.Models;
using CustomerProject.RepositoryLogics.Customer;
using CustomerProject.RepositoryLogics.CustomerTag;
using CustomerProject.RepositoryLogics.Email;
using CustomerProject.RepositoryLogics.MasterData;
using CustomerProject.RepositoryLogics.Phone;
using CustomerProject.RepositoryLogics.Tag;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IProvinceRepository, ProvinceRepositorySqlServer>();
builder.Services.AddScoped<IDistrictRepository, DistrictRepositorySqlServer>();
builder.Services.AddScoped<ISubDistrictRepository, SubDistrictRepositorySqlServer>();
builder.Services.AddScoped<IPostCodeRepository, PostCodeRepositorySqlServer>();
builder.Services.AddScoped<ICustomerRepository, CustomerRepositorySqlServer>();
builder.Services.AddScoped<IPhoneRepository, PhoneRepositorySqlServer>();
builder.Services.AddScoped<IEmailRepository, EmailRepositorySqlServer>();
builder.Services.AddScoped<ITagRepository, TagRepositorySqlServer>();
builder.Services.AddScoped<ICustomerTagRepository, CustomerTagRepositorySqlServer>();
builder.Services.AddScoped<CustomerOverview>();
builder.Services.AddScoped<CustomerManage>();
builder.Services.AddScoped<CustomerValidator>();
builder.Services.AddDbContext<CustomerExampleContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
        });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();
