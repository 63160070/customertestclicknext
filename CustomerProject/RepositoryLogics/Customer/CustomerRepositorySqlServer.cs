﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryLogics.Customer;
using CustomerProject.RepositoryModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CustomerProject.RepositoryLogics.Customer
{
    public class CustomerRepositorySqlServer : ICustomerRepository
    {
        private readonly CustomerExampleContext _context;

        public CustomerRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<CustomerModel> Queryable => _context.Customers.Select(x => new CustomerModel
        {
            CustomerId = x.CustomerId,
            CustomerType = x.CustomerType,
            PrefixName = x.PrefixName,
            CustomerCode = x.CustomerCode,
            FirstName = x.FirstName,
            LastName = x.LastName,
            IdCard = x.IdCard,
            Address = x.Address,
            Moo = x.Moo,
            Building = x.Building,
            Soi = x.Soi,
            Road = x.Road,
            SubDistrictId = x.SubDistrictId,
            SubDistrict = x.SubDistrict,
            DistrictId = x.DistrictId,
            District = x.District,
            ProvinceId = x.ProvinceId,
            Province = x.Province,
            PostCodeId = x.PostCodeId,
            PostCode = x.PostCode
        });

        public async Task Insert(CustomerModel model)
        {
            await _context.Customers.AddAsync(new CustomerProject.Database.Models.Customer
            {
                CustomerId = model.CustomerId,
                CustomerType = model.CustomerType,
                PrefixName = model.PrefixName,
                CustomerCode = model.CustomerCode,
                FirstName = model.FirstName,
                LastName = model.LastName,
                IdCard = model.IdCard,
                Address = model.Address,
                Moo = model.Moo,
                Building = model.Building,
                Soi = model.Soi,
                Road = model.Road,
                SubDistrictId = model.SubDistrictId,
                SubDistrict = model.SubDistrict,
                DistrictId = model.DistrictId,
                District = model.District,
                ProvinceId = model.ProvinceId,
                Province = model.Province,
                PostCodeId = model.PostCodeId,
                PostCode = model.PostCode
            });
        }

        public async Task InsertBulkCopy(DataTable dataTable)
        {
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "customer";
                    bulkCopy.BatchSize = 500;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
        }
    }
}
