﻿using CustomerProject.RepositoryModels;
using System.Data;

namespace CustomerProject.RepositoryLogics.Customer
{
    public interface ICustomerRepository
    {
        IQueryable<CustomerModel> Queryable { get; }
        Task Insert(CustomerModel model);
        Task InsertBulkCopy(DataTable dataTable);
    }
}
