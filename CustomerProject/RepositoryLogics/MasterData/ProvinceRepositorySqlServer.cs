﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public class ProvinceRepositorySqlServer : IProvinceRepository
    {
        private readonly CustomerExampleContext _context;

        public ProvinceRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<ProvinceModel> Queryable => _context.Provinces.Select(x => new ProvinceModel
        {
            ProvinceId = x.ProvinceId,
            Province1 = x.Province1,
        });
    }
}
