﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public class DistrictRepositorySqlServer : IDistrictRepository
    {
        private readonly CustomerExampleContext _context;

        public DistrictRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<DistrictModel> Queryable => _context.Districts.Select(x => new DistrictModel
        {
            DistrictId = x.DistrictId,
            District1 = x.District1,
            ProvinceId = x.ProvinceId
        });
    }
}
