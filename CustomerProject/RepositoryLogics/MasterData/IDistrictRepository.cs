﻿using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public interface IDistrictRepository
    {
        IQueryable<DistrictModel> Queryable { get; }
    }
}
