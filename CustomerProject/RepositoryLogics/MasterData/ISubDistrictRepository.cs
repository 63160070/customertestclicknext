﻿using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public interface ISubDistrictRepository
    {
        IQueryable<SubDistrictModel> Queryable { get; }
    }
}
