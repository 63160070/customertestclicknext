﻿using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public interface IPostCodeRepository
    {
        IQueryable<PostCodeModel> Queryable { get; }
    }
}
