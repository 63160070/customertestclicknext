﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public class SubDistrictRepositorySqlServer : ISubDistrictRepository
    {
        private readonly CustomerExampleContext _context;

        public SubDistrictRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<SubDistrictModel> Queryable => _context.SubDistricts.Select(x => new SubDistrictModel
        {
            SubDistrictId = x.SubDistrictId,
            SubDistrict1 = x.SubDistrict1,
            DistrictId = x.DistrictId
        });
    }
}
