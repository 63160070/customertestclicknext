﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public class PostCodeRepositorySqlServer : IPostCodeRepository
    {
        private readonly CustomerExampleContext _context;

        public PostCodeRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<PostCodeModel> Queryable => _context.PostCodes.Select(x => new PostCodeModel
        {
            PostCodeId = x.PostCodeId,
            PostCode1 = x.PostCode1,
            SubDistrictId = x.SubDistrictId
        });
    }
}
