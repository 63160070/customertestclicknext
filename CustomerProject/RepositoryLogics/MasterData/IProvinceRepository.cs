﻿using CustomerProject.RepositoryModels;

namespace CustomerProject.RepositoryLogics.MasterData
{
    public interface IProvinceRepository
    {
        IQueryable<ProvinceModel> Queryable { get; }
    }
}
