﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CustomerProject.RepositoryLogics.Phone
{
    public class PhoneRepositorySqlServer : IPhoneRepository
    {
        private readonly CustomerExampleContext _context;

        public PhoneRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<PhoneModel> Queryable => _context.Phones.Select(x => new PhoneModel
        {
            PhoneId = x.PhoneId,
            PhoneNumber = x.PhoneNumber,
            CustomerId = x.CustomerId
        });

        public async Task Insert(PhoneModel model)
        {
            await _context.Phones.AddAsync(new CustomerProject.Database.Models.Phone
            {
                PhoneId = model.PhoneId,
                PhoneNumber = model.PhoneNumber,
                CustomerId = model.CustomerId
            });
        }

        public async Task InsertBulkCopy(DataTable dataTable)
        {
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "phone";
                    bulkCopy.BatchSize = 500;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
        }
    }
}
