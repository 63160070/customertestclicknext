﻿using CustomerProject.RepositoryModels;
using System.Data;

namespace CustomerProject.RepositoryLogics.Phone
{
    public interface IPhoneRepository
    {
        IQueryable<PhoneModel> Queryable { get; }
        Task Insert(PhoneModel model);
        Task InsertBulkCopy(DataTable dataTable);
    }
}
