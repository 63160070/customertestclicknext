﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CustomerProject.RepositoryLogics.Tag
{
    public class TagRepositorySqlServer : ITagRepository
    {
        private readonly CustomerExampleContext _context;

        public TagRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<TagModel> Queryable => _context.Tags.Select(x => new TagModel
        {
            TagId = x.TagId,
            Tag1 = x.Tag1
        });

        public async Task Insert(TagModel model)
        {
            await _context.Tags.AddAsync(new CustomerProject.Database.Models.Tag
            {
                TagId = model.TagId,
                Tag1 = model.Tag1
            });
        }

        public async Task InsertBulkCopy(DataTable dataTable)
        {
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "tag";
                    bulkCopy.BatchSize = 500;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
        }
    }
}
