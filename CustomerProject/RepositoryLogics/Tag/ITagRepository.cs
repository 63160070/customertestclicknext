﻿using CustomerProject.RepositoryModels;
using System.Data;

namespace CustomerProject.RepositoryLogics.Tag
{
    public interface ITagRepository
    {
        IQueryable<TagModel> Queryable { get; }
        Task Insert(TagModel model);
        Task InsertBulkCopy(DataTable dataTable);
    }
}
