﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CustomerProject.RepositoryLogics.Email
{
    public class EmailRepositorySqlServer : IEmailRepository
    {
        private readonly CustomerExampleContext _context;

        public EmailRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<EmailModel> Queryable => _context.Emails.Select(x => new EmailModel
        {
            EmailId = x.EmailId,
            EmailAddress = x.EmailAddress,
            CustomerId = x.CustomerId
        });

        public async Task Insert(EmailModel model)
        {
            await _context.Emails.AddAsync(new CustomerProject.Database.Models.Email
            {
                EmailId = model.EmailId,
                EmailAddress = model.EmailAddress,
                CustomerId = model.CustomerId
            });
        }

        public async Task InsertBulkCopy(DataTable dataTable)
        {
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "email";
                    bulkCopy.BatchSize = 500;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
        }
    }
}
