﻿using CustomerProject.RepositoryModels;
using System.Data;

namespace CustomerProject.RepositoryLogics.Email
{
    public interface IEmailRepository
    {
        IQueryable<EmailModel> Queryable { get; }
        Task Insert(EmailModel model);
        Task InsertBulkCopy(DataTable dataTable);
    }
}
