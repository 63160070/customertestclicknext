﻿using CustomerProject.Database.Models;
using CustomerProject.RepositoryModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CustomerProject.RepositoryLogics.CustomerTag
{
    public class CustomerTagRepositorySqlServer : ICustomerTagRepository
    {
        private readonly CustomerExampleContext _context;

        public CustomerTagRepositorySqlServer(CustomerExampleContext context)
        {
            _context = context;
        }

        public IQueryable<CustomerTagModel> Queryable => _context.CustomerTags.Select(x => new CustomerTagModel
        {
            CustomerTagId = x.CustomerTagId,
            CustomerId = x.CustomerId,
            TagId = x.TagId,
        });

        public async Task Insert(CustomerTagModel model)
        {
            await _context.CustomerTags.AddAsync(new CustomerProject.Database.Models.CustomerTag
            {
                CustomerTagId = model.CustomerTagId,
                CustomerId = model.CustomerId,
                TagId = model.TagId
            });
        }

        public async Task InsertBulkCopy(DataTable dataTable)
        {
            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "customer_tag";
                    bulkCopy.BatchSize = 500;
                    bulkCopy.WriteToServer(dataTable);
                }
            }
        }
    }
}
