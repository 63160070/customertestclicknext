﻿using CustomerProject.RepositoryModels;
using System.Data;

namespace CustomerProject.RepositoryLogics.CustomerTag
{
    public interface ICustomerTagRepository
    {
        IQueryable<CustomerTagModel> Queryable { get; }
        Task Insert(CustomerTagModel model);
        Task InsertBulkCopy(DataTable dataTable);
    }
}
