﻿using CustomerProject.BusinessLogics;
using CustomerProject.BusinessModels;
using Microsoft.AspNetCore.Mvc;

namespace CustomerProject.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly CustomerManage customerManage;
        private readonly CustomerOverview customerOverview;

        public CustomerController(
              CustomerManage customerManage
            , CustomerOverview customerOverview
            )
        {
            this.customerManage = customerManage;
            this.customerOverview = customerOverview;
        }

        [HttpGet]
        public async Task<IActionResult> GenerateData()
        {
            var result = await customerManage.GenerateExcelFile();
            return result;
        }

        [HttpGet]
        public async Task<List<CustomerOverviewResponse>> Index()
        {
            var result = await customerOverview.Index();
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CustomerCreateRequest request)
        {
            await customerManage.Create(request);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> InsertData([FromForm]IFormFile file)
        {
            await customerManage.ImportExcelData(file);
            return Ok();
        }
    }
}
