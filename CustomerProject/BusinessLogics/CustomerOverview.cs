﻿using CustomerProject.BusinessModels;
using CustomerProject.RepositoryLogics.Customer;
using CustomerProject.RepositoryLogics.CustomerTag;
using CustomerProject.RepositoryLogics.Email;
using CustomerProject.RepositoryLogics.MasterData;
using CustomerProject.RepositoryLogics.Phone;
using CustomerProject.RepositoryLogics.Tag;
using Microsoft.EntityFrameworkCore;

namespace CustomerProject.BusinessLogics
{
    public class CustomerOverview
    {
        private readonly IProvinceRepository provinceRepository;
        private readonly IDistrictRepository districtRepository;
        private readonly ISubDistrictRepository subDistrictRepository;
        private readonly IPostCodeRepository postCodeRepository;
        private readonly ICustomerRepository customerRepository;
        private readonly IPhoneRepository phoneRepository;
        private readonly IEmailRepository emailRepository;
        private readonly ITagRepository tagRepository;
        private readonly ICustomerTagRepository customerTagRepository;

        public CustomerOverview(
              IProvinceRepository provinceRepository
            , IDistrictRepository districtRepository
            , ISubDistrictRepository subDistrictRepository
            , IPostCodeRepository postCodeRepository
            , ICustomerRepository customerRepository
            , IPhoneRepository phoneRepository
            , IEmailRepository emailRepository
            , ITagRepository tagRepository
            , ICustomerTagRepository customerTagRepository
        )
        {
            this.provinceRepository = provinceRepository;
            this.districtRepository = districtRepository;
            this.subDistrictRepository = subDistrictRepository;
            this.postCodeRepository = postCodeRepository;
            this.customerRepository = customerRepository;
            this.phoneRepository = phoneRepository;
            this.emailRepository = emailRepository;
            this.tagRepository = tagRepository;
            this.customerTagRepository = customerTagRepository;
        }

        public async Task<List<CustomerOverviewResponse>> Index()
        {
            var iQueryable = await customerRepository.Queryable.ToListAsync();
            var result = new List<CustomerOverviewResponse>();

            foreach (var customer in iQueryable)
            {
                var customerResponse = new CustomerOverviewResponse
                {
                    customer_type = (CustomerType)customer.CustomerType,
                    prefix_name = customer.PrefixName,
                    customer_code = customer.CustomerCode,
                    first_name = customer.FirstName,
                    last_name = customer.LastName,
                    id_card = customer.IdCard,
                    address = customer.Address,
                    moo = customer.Moo,
                    building = customer.Building,
                    soi = customer.Soi,
                    road = customer.Road,
                    sub_district = customer.SubDistrict,
                    district = customer.District,
                    province = customer.Province,
                    post_code = customer.PostCode,
                    phones = await phoneRepository.Queryable
                                                  .Where(p => p.CustomerId == customer.CustomerId)
                                                  .Select(p => p.PhoneNumber)
                                                  .ToListAsync(),
                    emails = await emailRepository.Queryable
                                                  .Where(e => e.CustomerId == customer.CustomerId)
                                                  .Select(e => e.EmailAddress)
                                                  .ToListAsync(),
                    //tags = await customerTagRepository.Queryable
                    //                                  .Where(ct => ct.CustomerId == customer.CustomerId)
                    //                                  .Join(tagRepository.Queryable, ct => ct.TagId, tag => tag.TagId, (ct, tag) => tag.TagName)
                    //                                  .ToListAsync()
                    tags = await (from ct in customerTagRepository.Queryable
                                  join t in tagRepository.Queryable on ct.TagId equals t.TagId
                                  where ct.CustomerId == customer.CustomerId
                                  select t.Tag1)
                                  .ToListAsync()
                };

                result.Add(customerResponse);
            }

            return result;
            //var results = new PagedDataResult<WorkflowIndexResponse>();
            //results.pageSize = request.pageSize;
            //results.rowCount = await iQueryable.CountAsync();
            //var data = await iQueryable.TakePage(request.pageIndex, request.pageSize, results.rowCount, out int actualPageIndex).ToListAsync();
            //results.pageIndex = actualPageIndex;

            //var iQueryableDataReferenced = workflowQueryBuilder.BuildDataReferencedQuery();
            //var dataDictionary = await (from dataReferenced in iQueryableDataReferenced
            //                            where data.Select(x => x.workflow_id).Contains(dataReferenced.id)
            //                            select dataReferenced).ToDictionaryAsync(x => x.id, x => x.is_referenced);

            //return this.BusinessResult(BusinessCode.Success, new PagedDataResult<WorkflowIndexResponse>
            //{
            //    pageIndex = results.pageIndex,
            //    pageSize = results.pageSize,
            //    rowCount = results.rowCount,
            //    data = data.Select(x => new WorkflowIndexResponse
            //    {
            //        workflow_id = x.workflow_id,
            //        code = x.code,
            //        name_foreign = x.name_foreign,
            //        name_local = x.name_local,
            //        is_active = x.is_active,
            //        is_referenced = dataDictionary[x.workflow_id],
            //    }).ToList()
            //});
        }
    }
}
