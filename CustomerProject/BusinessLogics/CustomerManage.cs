﻿using Bogus;
using ClosedXML.Excel;
using CustomerProject.BusinessModels;
using CustomerProject.Database.Models;
using CustomerProject.RepositoryLogics.Customer;
using CustomerProject.RepositoryLogics.CustomerTag;
using CustomerProject.RepositoryLogics.Email;
using CustomerProject.RepositoryLogics.MasterData;
using CustomerProject.RepositoryLogics.Phone;
using CustomerProject.RepositoryLogics.Tag;
using CustomerProject.RepositoryModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System.Data;
using static CustomerProject.BusinessLogics.CustomerValidator;

namespace CustomerProject.BusinessLogics
{
    public class CustomerManage
    {
        private readonly IProvinceRepository provinceRepository;
        private readonly IDistrictRepository districtRepository;
        private readonly ISubDistrictRepository subDistrictRepository;
        private readonly IPostCodeRepository postCodeRepository;
        private readonly ICustomerRepository customerRepository;
        private readonly IPhoneRepository phoneRepository;
        private readonly IEmailRepository emailRepository;
        private readonly ITagRepository tagRepository;
        private readonly ICustomerTagRepository customerTagRepository;
        private readonly CustomerExampleContext _context;
        private readonly CustomerValidator customerValidator;

        public CustomerManage(
              IProvinceRepository provinceRepository
            , IDistrictRepository districtRepository
            , ISubDistrictRepository subDistrictRepository
            , IPostCodeRepository postCodeRepository
            , ICustomerRepository customerRepository
            , IPhoneRepository phoneRepository
            , IEmailRepository emailRepository
            , ITagRepository tagRepository
            , ICustomerTagRepository customerTagRepository
            , CustomerExampleContext context
            , CustomerValidator customerValidator
            )
        {
            this.provinceRepository = provinceRepository;
            this.districtRepository = districtRepository;
            this.subDistrictRepository = subDistrictRepository;
            this.postCodeRepository = postCodeRepository;
            this.customerRepository = customerRepository;
            this.phoneRepository = phoneRepository;
            this.emailRepository = emailRepository;
            this.tagRepository = tagRepository;
            this.customerTagRepository = customerTagRepository;
            this._context = context;
            this.customerValidator = customerValidator;
        }

        public async Task<IActionResult> GenerateExcelFile()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Customers");
                var headers = new List<string> {
                    "customer_type", "prefix_name", "customer_code", "first_name", "last_name",
                    "address", "moo", "building", "soi", "road", "sub_district", "district", "province",
                    "post_code", "id_card", "phone", "email", "tag"
                };

                foreach (var header in headers)
                {
                    var index = headers.IndexOf(header);
                    worksheet.Cell(1, index + 1).Value = header;
                }

                var customerFaker = new Faker<ExcelGenModel>()
                                        .StrictMode(false)
                                        .RuleFor(c => c.CustomerType, f => f.PickRandom<CustomerType>())
                                        .RuleFor(c => c.PrefixName, f => f.Name.Prefix())
                                        .RuleFor(c => c.CustomerCode, f => f.Random.Replace("CU-###-##-####"))
                                        .RuleFor(c => c.FirstName, f => f.Name.FirstName())
                                        .RuleFor(c => c.LastName, f => f.Name.LastName())
                                        .RuleFor(c => c.Address, f => f.Address.StreetAddress())
                                        .RuleFor(c => c.Moo, f => f.Address.SecondaryAddress())
                                        .RuleFor(c => c.Building, f => f.Company.CompanyName())
                                        .RuleFor(c => c.Soi, f => f.Random.Number(1, 99).ToString())
                                        .RuleFor(c => c.Road, f => f.Address.StreetName())
                                        .RuleFor(c => c.SubDistrict, f => f.Address.StreetName())
                                        .RuleFor(c => c.District, f => f.Address.City())
                                        .RuleFor(c => c.Province, f => f.Address.State())
                                        .RuleFor(c => c.PostCode, f => f.Random.ReplaceNumbers("#####"))
                                        .RuleFor(c => c.IdCard, f => f.Random.ReplaceNumbers("#############"))
                                        .RuleFor(c => c.Phone, f => Enumerable.Range(0, f.Random.Int(2, 5))
                                                                              .Select(_ => f.Random.ReplaceNumbers("#############"))
                                                                              .ToList())
                                        .RuleFor(c => c.Email, f => Enumerable.Range(0, f.Random.Int(2, 5))
                                                                              .Select(_ => f.Internet.Email())
                                                                              .ToList())
                                        .RuleFor(c => c.Tag, f => f.Random.Shuffle(new[] { "TAG_1", "TAG_2", "TAG_3", "TAG_4", "TAG_5" })
                                                                                   .Take(2)
                                                                                   .ToList());

                var customers = customerFaker.Generate(5000);

                int currentRow = 2;
                foreach (var customer in customers)
                {
                    worksheet.Cell(currentRow, 1).Value = customer.CustomerType == CustomerType.Person ? "บุคคล" : "บริษัท";
                    worksheet.Cell(currentRow, 2).Value = customer.PrefixName;
                    worksheet.Cell(currentRow, 3).Value = customer.CustomerCode;
                    worksheet.Cell(currentRow, 4).Value = customer.FirstName;
                    worksheet.Cell(currentRow, 5).Value = customer.LastName;
                    worksheet.Cell(currentRow, 6).Value = customer.Address;
                    worksheet.Cell(currentRow, 7).Value = customer.Moo;
                    worksheet.Cell(currentRow, 8).Value = customer.Building;
                    worksheet.Cell(currentRow, 9).Value = customer.Soi;
                    worksheet.Cell(currentRow, 10).Value = customer.Road;
                    worksheet.Cell(currentRow, 11).Value = customer.SubDistrict;
                    worksheet.Cell(currentRow, 12).Value = customer.District;
                    worksheet.Cell(currentRow, 13).Value = customer.Province;
                    worksheet.Cell(currentRow, 14).Value = customer.PostCode;
                    worksheet.Cell(currentRow, 15).Value = customer.IdCard;
                    worksheet.Cell(currentRow, 16).Value = string.Join(",", customer.Phone!);
                    worksheet.Cell(currentRow, 17).Value = string.Join(",", customer.Email!);
                    worksheet.Cell(currentRow, 18).Value = string.Join(",", customer.Tag!);
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return new FileContentResult(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        FileDownloadName = "Customers.xlsx"
                    };
                }
            }
        }

        public async Task Create(CustomerCreateRequest request)
        {
            #region validate

            var checkDuplicatedCustomerCode = await customerValidator.CheckDuplicateCustomer(new BaseCheckDuplicateInputModel() { field = nameof(CustomerModel.CustomerCode), value = request.customer_code });
            if (checkDuplicatedCustomerCode == true)
                throw new Exception($"Code {request.customer_code} is duplicated in the system.");

            var checkDuplicatedIdCard = await customerValidator.CheckDuplicateCustomer(new BaseCheckDuplicateInputModel() { field = nameof(CustomerModel.IdCard), value = request.id_card });
            if (checkDuplicatedIdCard == true)
                throw new Exception($"Id Card {request.id_card} is duplicated in the system.");

            foreach (var phone in request.phones)
            {
                var checkDuplicatedPhone = await customerValidator.CheckDuplicatePhone(new BaseCheckDuplicateInputModel() { field = nameof(PhoneModel.PhoneNumber), value = phone });
                if (checkDuplicatedPhone == true)
                    throw new Exception($"Phone number {phone} is duplicated in the system.");
            }

            foreach (var email in request.emails)
            {
                var checkDuplicatedEmail = await customerValidator.CheckDuplicateEmail(new BaseCheckDuplicateInputModel() { field = nameof(EmailModel.EmailAddress), value = email });
                if (checkDuplicatedEmail == true)
                    throw new Exception($"Email {email} is duplicated in the system.");
            }

            if (request.tags != null)
            {
                foreach (var tag in request.tags)
                {
                    var checkDuplicatedTag = await customerValidator.CheckDuplicateTag(new BaseCheckDuplicateInputModel() { field = nameof(TagModel.Tag1), value = tag });
                    if (checkDuplicatedTag == true)
                        throw new Exception($"Tag {tag} is duplicated in the system.");
                }
            }

            #endregion

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Guid customer_id = Guid.NewGuid();
                    await customerRepository.Insert(new CustomerModel
                    {
                        CustomerId = customer_id,
                        CustomerType = (int)request.customer_type,
                        PrefixName = request.prefix_name,
                        CustomerCode = request.customer_code,
                        FirstName = request.first_name,
                        LastName = request.last_name,
                        IdCard = request.id_card,
                        Address = request.address,
                        Moo = request.moo,
                        Building = request.building,
                        Soi = request.soi,
                        Road = request.road,
                        SubDistrictId = request.sub_district_id,
                        SubDistrict = request.sub_district,
                        DistrictId = request.district_id,
                        District = request.district,
                        ProvinceId = request.province_id,
                        Province = request.province,
                        PostCodeId = request.post_code_id,
                        PostCode = request.post_code
                    });

                    foreach(var phone in request.phones)
                    {
                        Guid phone_id = Guid.NewGuid();
                        await phoneRepository.Insert(new PhoneModel
                        {
                            PhoneId = phone_id,
                            PhoneNumber = phone,
                            CustomerId = customer_id,
                        });
                    }

                    foreach(var email in request.emails)
                    {
                        Guid email_id = Guid.NewGuid();
                        await emailRepository.Insert(new EmailModel
                        {
                            EmailId = email_id,
                            EmailAddress = email,
                            CustomerId = customer_id,
                        });
                    }

                    if(request.tags != null)
                    {
                        foreach (var tag in request.tags)
                        {
                            Guid tag_id = Guid.NewGuid();
                            await tagRepository.Insert(new TagModel
                            {
                                TagId = tag_id,
                                Tag1 = tag,
                            });

                            Guid customerTag_id = Guid.NewGuid();
                            await customerTagRepository.Insert(new CustomerTagModel
                            {
                                CustomerTagId = customerTag_id,
                                TagId = tag_id,
                                CustomerId = customer_id
                            });
                        }
                    }

                    if(request.tag_ids != null)
                    {
                        foreach(var tagId in request.tag_ids)
                        {
                            Guid customerTag_id = Guid.NewGuid();
                            await customerTagRepository.Insert(new CustomerTagModel
                            {
                                CustomerTagId = customerTag_id,
                                TagId = tagId,
                                CustomerId = customer_id
                            });
                        }
                    }
                    _context.SaveChanges();
                    await dbContextTransaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    await dbContextTransaction.RollbackAsync();
                }
            }

        }

        public async Task ImportExcelData(IFormFile file)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var customers = new List<CustomerImportModel>();
            var phones = new List<PhoneImportModel>();
            var emails = new List<EmailImportModel>();
            var tags = new List<TagImportModel>();
            var customerTags = new List<CustomerTagImportModel>();

            var provinceData = await provinceRepository.Queryable.ToListAsync();
            var districtData = await districtRepository.Queryable.ToListAsync();
            var subDistrictData = await subDistrictRepository.Queryable.ToListAsync();
            var postCodeData = await postCodeRepository.Queryable.ToListAsync();
            var tagData = await tagRepository.Queryable.ToListAsync();

            using (var stream = file.OpenReadStream())
            {
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0];
                    for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                    {
                        var customerTypeCol = worksheet.Cells[row, 1].Value.ToString()?.Trim() == "บุคคล" ? CustomerType.Person : CustomerType.Company;
                        var prefixNameCol = worksheet.Cells[row, 2].Value.ToString()?.Trim();
                        var customerCodeCol = worksheet.Cells[row, 3].Value.ToString()?.Trim();
                        var firstNameCol = worksheet.Cells[row, 4].Value.ToString()?.Trim();
                        var lastNameCol = worksheet.Cells[row, 5].Value?.ToString()?.Trim();
                        var addressCol = worksheet.Cells[row, 6].Value.ToString()?.Trim();
                        var mooCol = worksheet.Cells[row, 7].Value?.ToString()?.Trim();
                        var buildingCol = worksheet.Cells[row, 8].Value?.ToString()?.Trim();
                        var soiCol = worksheet.Cells[row, 9].Value?.ToString()?.Trim();
                        var roadCol = worksheet.Cells[row, 10].Value?.ToString()?.Trim();
                        var subDistrictCol = worksheet.Cells[row, 11].Value?.ToString()?.Trim();
                        var disrtictCol = worksheet.Cells[row, 12].Value?.ToString()?.Trim();
                        var provinceCol = worksheet.Cells[row, 13].Value?.ToString()?.Trim();
                        var postCodeCol = worksheet.Cells[row, 14].Value?.ToString()?.Trim();
                        var idCardCol = worksheet.Cells[row, 15].Value.ToString()?.Trim();
                        var phoneCol = worksheet.Cells[row, 16].Value?.ToString()?.Trim();
                        var emailCol = worksheet.Cells[row, 17].Value?.ToString()?.Trim();
                        var tagCol = worksheet.Cells[row, 18].Value?.ToString()?.Trim();

                        var customer = new CustomerImportModel
                        {
                            CustomerId = Guid.NewGuid(),
                            CustomerType = (int)customerTypeCol,
                            PrefixName = prefixNameCol,
                            CustomerCode = customerCodeCol,
                            FirstName = firstNameCol,
                            LastName = lastNameCol,
                            IdCard = idCardCol,
                            Address = addressCol,
                            Moo = mooCol,
                            Building = buildingCol,
                            Soi = soiCol,
                            Road = roadCol,
                            SubDistrict = subDistrictCol,
                            District = disrtictCol,
                            Province = provinceCol,
                            PostCode = postCodeCol,
                        };

                        if (!String.IsNullOrWhiteSpace(provinceCol))
                        {
                            customer.ProvinceId = provinceData.Where(x => x.Province1 == provinceCol)
                                                              .Select(x => (int?)x.ProvinceId)
                                                              .SingleOrDefault();
                        }

                        if (customer.ProvinceId != null)
                        {
                            var districtList = districtData.Where(x => x.District1 == disrtictCol)
                                                           .ToList();

                            customer.DistrictId = districtList.Where(x => x.ProvinceId == customer.ProvinceId)
                                                              .Select(x => (int?)x.DistrictId)
                                                              .SingleOrDefault();
                        }

                        if (customer.DistrictId != null)
                        {
                            var subDistrictList = subDistrictData.Where(x => x.SubDistrict1 == subDistrictCol)
                                                                 .ToList();

                            customer.SubDistrictId = subDistrictList.Where(x => x.DistrictId == customer.DistrictId)
                                                                    .Select(x => (int?)x.SubDistrictId)
                                                                    .SingleOrDefault();
                        }

                        if (customer.SubDistrictId != null)
                        {
                            var postCodeList = postCodeData.Where(x => x.PostCode1 == postCodeCol)
                                                           .ToList();

                            customer.PostCodeId = postCodeList.Where(x => x.SubDistrictId == customer.SubDistrictId)
                                                              .Select(x => (int?)x.PostCodeId)
                                                              .SingleOrDefault();
                        }
                        customers.Add(customer);

                        List<string> phoneNumbers = phoneCol.Split(',').Select(s => s.Trim()).ToList();
                        var phone = new PhoneImportModel
                        {
                            PhoneNumbers = phoneNumbers,
                            CustomerId = customer.CustomerId
                        };
                        phones.Add(phone);

                        List<string> emailAddresses = emailCol.Split(',').Select(s => s.Trim()).ToList();
                        var email = new EmailImportModel
                        {
                            EmailAddresses = emailAddresses,
                            CustomerId = customer.CustomerId
                        };
                        emails.Add(email);

                        List<string> tagNames = tagCol.Split(',').Select(s => s.Trim()).ToList();
                        foreach (var t in tagNames)
                        {
                            var existingDBTag = tagData.SingleOrDefault(tag => tag.Tag1 == t);
                            var existingCurrentTag = tags.SingleOrDefault(tag => tag.Tag == t);

                            if (existingCurrentTag == null && existingDBTag == null)
                            {
                                var newTag = new TagImportModel
                                {
                                    TagId = Guid.NewGuid(),
                                    Tag = t
                                    //Tag = "888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888"

                                };

                                var cusTag = new CustomerTagImportModel
                                {
                                    CustomerId = customer.CustomerId,
                                    TagId = newTag.TagId
                                };
                                tags.Add(newTag);
                                customerTags.Add(cusTag);
                            }
                            else
                            {
                                var cusTag = new CustomerTagImportModel
                                {
                                    CustomerId = customer.CustomerId,
                                    TagId = existingDBTag == null ? existingCurrentTag.TagId : existingDBTag.TagId
                                };
                                customerTags.Add(cusTag);
                            }
                        }
                    }
                }
            }
            #region CustomerDataTable
            DataTable customerDataTable = new DataTable("Customer");
            customerDataTable.Columns.Add("CustomerId", typeof(Guid));
            customerDataTable.Columns.Add("CustomerType", typeof(int));
            customerDataTable.Columns.Add("PrefixName", typeof(string));
            customerDataTable.Columns.Add("CustomerCode", typeof(string));
            customerDataTable.Columns.Add("FirstName", typeof(string));
            customerDataTable.Columns.Add("LastName", typeof(string));
            customerDataTable.Columns.Add("IdCard", typeof(string));
            customerDataTable.Columns.Add("Address", typeof(string));
            customerDataTable.Columns.Add("Moo", typeof(string));
            customerDataTable.Columns.Add("Building", typeof(string));
            customerDataTable.Columns.Add("Soi", typeof(string));
            customerDataTable.Columns.Add("Road", typeof(string));
            customerDataTable.Columns.Add("SubDistrictId", typeof(int));
            customerDataTable.Columns.Add("SubDistrict", typeof(string));
            customerDataTable.Columns.Add("DistrictId", typeof(int));
            customerDataTable.Columns.Add("District", typeof(string));
            customerDataTable.Columns.Add("ProvinceId", typeof(int));
            customerDataTable.Columns.Add("Province", typeof(string));
            customerDataTable.Columns.Add("PostCodeId", typeof(int));
            customerDataTable.Columns.Add("PostCode", typeof(string));
            foreach (var customer in customers)
            {
                customerDataTable.Rows.Add(customer.CustomerId
                                         , customer.CustomerType
                                         , customer.PrefixName
                                         , customer.CustomerCode
                                         , customer.FirstName
                                         , customer.LastName
                                         , customer.IdCard
                                         , customer.Address
                                         , customer.Moo
                                         , customer.Building
                                         , customer.Soi
                                         , customer.Road
                                         , customer.SubDistrictId
                                         , customer.SubDistrict
                                         , customer.DistrictId
                                         , customer.District
                                         , customer.ProvinceId
                                         , customer.Province
                                         , customer.PostCodeId
                                         , customer.PostCode
                                         );
            }
            #endregion

            #region PhoneDataTable
            DataTable phoneDataTable = new DataTable("Phone");
            phoneDataTable.Columns.Add("PhoneId", typeof(Guid));
            phoneDataTable.Columns.Add("PhoneNumber", typeof(string));
            phoneDataTable.Columns.Add("CustomerId", typeof(Guid));
            foreach (var phone in phones)
            {
                foreach (var phoneNumber in phone.PhoneNumbers)
                {
                    PhoneModel phoneRepo = new PhoneModel
                    {
                        PhoneId = Guid.NewGuid(),
                        PhoneNumber = phoneNumber,
                        CustomerId = phone.CustomerId,
                    };
                    phoneDataTable.Rows.Add(phoneRepo.PhoneId
                                          , phoneRepo.PhoneNumber
                                          , phoneRepo.CustomerId
                                          );
                }
            }
            #endregion

            #region EmailDataTable
            DataTable emailDataTable = new DataTable("Email");
            emailDataTable.Columns.Add("EmailId", typeof(Guid));
            emailDataTable.Columns.Add("EmailAddress", typeof(string));
            emailDataTable.Columns.Add("CustomerId", typeof(Guid));
            foreach (var email in emails)
            {
                foreach (var emailAddress in email.EmailAddresses)
                {
                    EmailModel emailRepo = new EmailModel
                    {
                        EmailId = Guid.NewGuid(),
                        EmailAddress = emailAddress,
                        CustomerId = email.CustomerId,
                    };
                    emailDataTable.Rows.Add(emailRepo.EmailId
                                          , emailRepo.EmailAddress
                                          , emailRepo.CustomerId
                                          );
                }
            }
            #endregion

            #region TagDataTable
            DataTable tagDataTable = new DataTable("Tag");
            tagDataTable.Columns.Add("TagId", typeof(Guid));
            tagDataTable.Columns.Add("Tag1", typeof(string));
            foreach (var tag in tags)
            {
                tagDataTable.Rows.Add(tag.TagId
                                    , tag.Tag
                                    );
            }
            #endregion

            #region CustomerTagDataTable
            DataTable customerTagDataTable = new DataTable("CustomerTag");
            customerTagDataTable.Columns.Add("CustomerTagId", typeof(Guid));
            customerTagDataTable.Columns.Add("CustomerId", typeof(Guid));
            customerTagDataTable.Columns.Add("TagId", typeof(Guid));
            foreach (var customerTag in customerTags)
            {
                customerTag.CustomerTagId = Guid.NewGuid();
                customerTagDataTable.Rows.Add(customerTag.CustomerTagId
                                            , customerTag.CustomerId
                                            , customerTag.TagId
                                            );
            }
            #endregion

            using (var connection = new SqlConnection(_context.Database.GetDbConnection().ConnectionString))
            {
                connection.Open();
                await using (var dbContextTransaction = connection.BeginTransaction())
                {
                    using (var bulkCopy = new SqlBulkCopy(connection,SqlBulkCopyOptions.Default,dbContextTransaction))
                    {
                        bulkCopy.BatchSize = 500;
                        try
                        {
                            bulkCopy.DestinationTableName = "customer";
                            bulkCopy.WriteToServer(customerDataTable);
                            bulkCopy.DestinationTableName = "phone";
                            bulkCopy.WriteToServer(phoneDataTable);
                            bulkCopy.DestinationTableName = "email";
                            bulkCopy.WriteToServer(emailDataTable);
                            bulkCopy.DestinationTableName = "tag";
                            bulkCopy.WriteToServer(tagDataTable);
                            bulkCopy.DestinationTableName = "customer_tag";
                            bulkCopy.WriteToServer(customerTagDataTable);
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
            }

            #region RepoInsert
            //using (var dbContextTransaction = _context.Database.BeginTransaction())
            //{
            //    try
            //    {
            //        foreach (var customer in customers)
            //        {
            //            CustomerModel customerRepo = new CustomerModel
            //            {
            //                CustomerId = customer.CustomerId,
            //                CustomerType = customer.CustomerType,
            //                PrefixName = customer.PrefixName,
            //                CustomerCode = customer.CustomerCode,
            //                FirstName = customer.FirstName,
            //                LastName = customer.LastName,
            //                IdCard = customer.IdCard,
            //                Address = customer.Address,
            //                Moo = customer.Moo,
            //                Building = customer.Building,
            //                Soi = customer.Soi,
            //                Road = customer.Road,
            //                SubDistrictId = customer.SubDistrictId,
            //                DistrictId = customer.DistrictId,
            //                ProvinceId = customer.ProvinceId,
            //                PostCodeId = customer.PostCodeId,
            //                IsThai = customer.IsThai
            //            };
            //            await customerRepository.Insert(customerRepo);
            //        }

            //        foreach (var phone in phones)
            //        {
            //            foreach (var phoneNumber in phone.PhoneNumbers)
            //            {
            //                PhoneModel phoneRepo = new PhoneModel
            //                {
            //                    PhoneId = Guid.NewGuid(),
            //                    PhoneNumber = phoneNumber,
            //                    CustomerId = phone.CustomerId,
            //                };
            //                await phoneRepository.Insert(phoneRepo);
            //            }
            //        }

            //        foreach (var email in emails)
            //        {
            //            foreach (var emailAddress in email.EmailAddresses)
            //            {
            //                EmailModel emailRepo = new EmailModel
            //                {
            //                    EmailId = Guid.NewGuid(),
            //                    EmailAddress = emailAddress,
            //                    CustomerId = email.CustomerId,
            //                };
            //                await emailRepository.Insert(emailRepo);
            //            }
            //        }

            //        foreach (var tag in tags)
            //        {
            //            TagModel tagRepo = new TagModel
            //            {
            //                TagId = tag.TagId,
            //                Tag1 = tag.Tag
            //            };
            //            await tagRepository.Insert(tagRepo);
            //        }

            //        foreach (var customerTag in customerTags)
            //        {
            //            CustomerTagModel customerTagRepo = new CustomerTagModel
            //            {
            //                CustomerTagId = customerTag.CustomerTagId,
            //                CustomerId = customerTag.CustomerId,
            //                TagId = customerTag.TagId
            //            };
            //            await customerTagRepository.Insert(customerTagRepo);
            //        }

            //        await dbContextTransaction.CommitAsync();
            //        _context.SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {
            //        await dbContextTransaction.RollbackAsync();
            //    }
            //}
            #endregion
        }
    }
}
