﻿using CustomerProject.RepositoryLogics.Customer;
using CustomerProject.RepositoryLogics.CustomerTag;
using CustomerProject.RepositoryLogics.Email;
using CustomerProject.RepositoryLogics.MasterData;
using CustomerProject.RepositoryLogics.Phone;
using CustomerProject.RepositoryLogics.Tag;
using CustomerProject.RepositoryModels;
using Microsoft.EntityFrameworkCore;

namespace CustomerProject.BusinessLogics
{
    public class CustomerValidator
    {
        private readonly ICustomerRepository customerRepository;
        private readonly IPhoneRepository phoneRepository;
        private readonly IEmailRepository emailRepository;
        private readonly ITagRepository tagRepository;
        private readonly ICustomerTagRepository customerTagRepository;

        public CustomerValidator(
              ICustomerRepository customerRepository
            , IPhoneRepository phoneRepository
            , IEmailRepository emailRepository
            , ITagRepository tagRepository
            , ICustomerTagRepository customerTagRepository
        )
        {
            this.customerRepository = customerRepository;
            this.phoneRepository = phoneRepository;
            this.emailRepository = emailRepository;
            this.tagRepository = tagRepository;
            this.customerTagRepository = customerTagRepository;
        }

        public async Task<bool> CheckDuplicateCustomer(BaseCheckDuplicateInputModel request)
        {
            var customerIQueryable = customerRepository.Queryable;

            if (request.field == nameof(CustomerModel.CustomerCode))
                customerIQueryable = customerIQueryable.Where(w => w.CustomerCode.ToLower() == request.value.ToLower());

            if (request.field == nameof(CustomerModel.IdCard))
                customerIQueryable = customerIQueryable.Where(w => w.IdCard.ToLower() == request.value.ToLower());

            //if (request.id.HasValue)
            //    iQueryable = iQueryable.Where(w => w.workflow_id != request.id);

            var isDuplicated = await customerIQueryable.AnyAsync();

            return isDuplicated;
        }

        public async Task<bool> CheckDuplicatePhone(BaseCheckDuplicateInputModel request)
        {
            var isDuplicated = await phoneRepository.Queryable.Where(w => w.PhoneNumber == request.value).AnyAsync(); ;
            return isDuplicated;
        }

        public async Task<bool> CheckDuplicateEmail(BaseCheckDuplicateInputModel request)
        {
            var isDuplicated = await emailRepository.Queryable.Where(w => w.EmailAddress == request.value).AnyAsync(); ;
            return isDuplicated;
        }
        public async Task<bool> CheckDuplicateTag(BaseCheckDuplicateInputModel request)
        {
            var isDuplicated = await tagRepository.Queryable.Where(w => w.Tag1 == request.value).AnyAsync(); ;
            return isDuplicated;
        }
        public class BaseCheckDuplicateInputModel
        {
            public Guid? id { get; set; }
            public string field { get; set; }
            public string value { get; set; } = null!;
        }
    }
}
