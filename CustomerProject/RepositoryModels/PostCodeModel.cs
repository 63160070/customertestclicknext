﻿namespace CustomerProject.RepositoryModels
{
    public class PostCodeModel
    {
        public int PostCodeId { get; set; }
        public string PostCode1 { get; set; } = null!;
        public int SubDistrictId { get; set; }
    }
}
