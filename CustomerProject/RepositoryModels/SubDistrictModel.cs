﻿namespace CustomerProject.RepositoryModels
{
    public class SubDistrictModel
    {
        public int SubDistrictId { get; set; }
        public string SubDistrict1 { get; set; } = null!;
        public int DistrictId { get; set; }
    }
}
