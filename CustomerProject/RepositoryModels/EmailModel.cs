﻿namespace CustomerProject.RepositoryModels
{
    public class EmailModel
    {
        public Guid EmailId { get; set; }
        public string EmailAddress { get; set; } = null!;
        public Guid CustomerId { get; set; }
    }
}
