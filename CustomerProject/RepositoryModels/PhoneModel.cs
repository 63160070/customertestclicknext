﻿namespace CustomerProject.RepositoryModels
{
    public class PhoneModel
    {
        public Guid PhoneId { get; set; }
        public string PhoneNumber { get; set; } = null!;
        public Guid CustomerId { get; set; }
    }
}
