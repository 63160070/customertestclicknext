﻿namespace CustomerProject.RepositoryModels
{
    public class CustomerTagModel
    {
        public Guid CustomerTagId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid TagId { get; set; }
    }
}
