﻿namespace CustomerProject.RepositoryModels
{
    public class DistrictModel
    {
        public int DistrictId { get; set; }
        public string District1 { get; set; } = null!;
        public int ProvinceId { get; set; }
    }
}
