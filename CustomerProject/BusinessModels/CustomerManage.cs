﻿namespace CustomerProject.BusinessModels
{
    public class CustomerCreateOrUpdateRequest
    {
        public CustomerType customer_type { get; set; }
        public string prefix_name { get; set; } = null!;
        public string customer_code { get; set; } = null!;
        public string first_name { get; set; } = null!;
        public string? last_name { get; set; }
        public string id_card { get; set; } = null!;
        public string address { get; set; } = null!;
        public string? moo { get; set; }
        public string? building { get; set; }
        public string? soi { get; set; }
        public string? road { get; set; }
        public int? sub_district_id { get; set; }
        public string? sub_district { get; set; }
        public int? district_id { get; set; }
        public string? district { get; set; }
        public int? province_id { get; set; }
        public string? province { get; set; }
        public int? post_code_id { get; set; }
        public string? post_code { get; set; }
        public List<string> phones { get; set; } = null!;
        public List<string> emails { get; set; } = null!;
        public List<Guid>? tag_ids { get; set; }
        public List<string>? tags { get; set; }
    }

    public class CustomerCreateRequest : CustomerCreateOrUpdateRequest
    {
    }
}
