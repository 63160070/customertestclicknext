﻿namespace CustomerProject.BusinessModels
{
    public class ExcelGenModel
    {
        public CustomerType CustomerType { get; set; }
        public string PrefixName { get; set; }
        public string CustomerCode { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string? LastName { get; set; }
        public string Address { get; set; } = null!;
        public string? Moo { get; set; }
        public string? Building { get; set; }
        public string? Soi { get; set; }
        public string? Road { get; set; }
        public string? SubDistrict { get; set; }
        public string? District { get; set; }
        public string? Province { get; set; }
        public string? PostCode { get; set; }
        public string IdCard { get; set; } = null!;
        public List<string> Phone { get; set; } = null!;
        public List<string> Email { get; set; } = null!;
        public List<string> Tag { get; set; } = null!;
    }

    public enum CustomerType
    {
        Person = 1,
        Company = 2
    }
}
